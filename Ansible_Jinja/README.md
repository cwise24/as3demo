# Ansible Versions using AS3

In this folder, you  can unlock the power of Jinja within your Ansible driven deployments
## Jinja2 templates

The **j2** folder holds our AS3 declaration used in the `http.yml` playbook.

## Files

*j2* Jinja2 template directory

*http.yml* Playbook used to deploy virtual server

*var_file.yml* External variable file defining our user Auth and virtual server to be built


## Ansible Playbook

Ansible play to use external variables

`ansible-playbook -i inventory http.yml -e "@var_file.yml"`

Steps in the playbook:
- Wait for Big IP to be ready
- Generate login token
- Apply variables to jinja template