## Formatting Certificates

You will have to format your certificate and key files so they are 'flat' as the standard .pem format will fail.

`cat <file.crt/key> | sed -z 's/\n/\\n/g' > newFile.crt/key`

# Create a SecureVault cryptogram for an AS3 declaration


This document shows/explains steps needed to convert your certificate/keys to be formated for AS3 (json).

[K14869003](https://support.f5.com/csp/article/K14869003)

For the tl;dr folks:

Convert your passphrase to base 64 encoded:
```
echo -n "superSecretPassphrase" | base64
c3VwZXJTZWNyZXRQYXNzcGhyYXNl
```

If there is a trailing ``=`` **remove** it

Convert cert/key to json format for AS3:

```
jq -Rs . < <certificate or key file>
```

One other helpful tool with openssl to test your passphrase with your key file

```
openssl rsa -check -in <private_key_file>
Enter pass phrase for <private_key_file>:
superSecretPassphrase
```
