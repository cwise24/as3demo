# iControl AS3 Templates BIG-IQ

REST [API reference](https://clouddocs.f5.com/products/big-iq/mgmt-api/v0.0/ApiReferences/bigiq_public_api_ref/r_public_api_references.html)

REST endpoints for [service catalog](https://clouddocs.f5.com/products/big-iq/mgmt-api/v0.0/ApiReferences/bigiq_public_api_ref/r_adc_template_state.html#post-mgmt-cm-global-templates)

## Service Catalog Depricated
[K87924313](https://support.f5.com/csp/article/K87924313)   

The original Application Templates on BIG-IQ (introduced in v6.0) provided a graphical UI that allowed administrators to easily templatize and deploy applications. However, this initial implementation of Application Templates on BIG-IQ was not compatible with AS3. Those templates are called on BIG-IQ “Service Catalog Templates”.

F5 highly recommend to use AS3 Application Templates over the original Application Templates as they are not being enhanced going forward.

If you have existing application services deployed with the original Application Templates (BIG-IQ Service Catalog), you could either translate and re-deploy it using AS3 or convert it to an Legacy Application Service on BIG-IQ.

More information is also available on the following knowledge article [K87924313](https://support.f5.com/csp/article/K87924313).   

Service Catalog Template [link](https://techdocs.f5.com/en-us/bigiq-8-0-0/monitoring-managing-applications-using-big-iq/manage-big-iq-service-templates.html)

Managing AS3 Templates [link](https://techdocs.f5.com/en-us/bigiq-8-0-0/monitoring-managing-applications-using-big-iq/manage-big-iq-as3-templates.html)

## Using iControl to Import/Export AS3 Templates

Please follow [K25492181](https://support.f5.com/csp/article/K25492181) 

## Using iControl to update AS3 Templates

Step 1. Rest call to get AS3 template id, name, generation and published status
```
curl -s -H "Content-Type: application/json" http://localhost:8100/cm/global/appsvcs-templates/ | jq ".items[] | {id,name,generation,published}"
```
Example output
```
{
  "id": "bb508779-0dc2-3d54-b6c9-260fe7263085",
  "name": "example-https-to-http-n-nodewise-v2022",
  "generation": 12,
  "published": true
}
```

Step 2. Once you have the id, you can issue this command to `unpublish` the template. A publised template is **read-only**  

Exert from [Edit an AS3 Template](https://techdocs.f5.com/en-us/bigiq-8-0-0/monitoring-managing-applications-using-big-iq/manage-big-iq-as3-templates.html)   

You cannot edit a published template. If the template has been published, but has not been used to deploy an application, you can unpublish it to make it writable. If the template has been used to deploy an application, you have two options:
- Make a clone of the published template and make your changes to the clone. For details, refer to Clone an AS3 template on support.f5.com..
- Use the Switch to template button to change the template that the application uses. For details, refer to Change the template for a deployed application on support.f5.com.
```
curl -s -H "Content-Type: application/json" http://localhost:8100/cm/global/appsvcs-templates/bb508779-0dc2-3d54-b6c9-260fe7263085 -X PATCH -d "{'published': 'false'}"  | jq .
```

Step 3. Now that the template is unpublished you can make edits and make note to update **generation** number. Push edited file back to BIG-IQ
```
curl -s -H "Content-Type: application/json" http://localhost:8100/cm/global/appsvcs-templates/bb508779-0dc2-3d54-b6c9-260fe7263085 -X PUT -d @nbcu-anow-https-to-http-n-nodewise-v2022.json  | jq .
```

